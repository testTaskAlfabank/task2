import net.bytebuddy.implementation.bytecode.Throw;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.List;

public class task2_2 {
    WebDriver webDriver;
    WebDriverWait wait;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "E:\\drivers\\chromedriver.exe");
        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 30, 500);
        webDriver.manage().window().maximize();
        webDriver.get("https://yandex.ru");
        webDriver.findElement(By.linkText("Маркет")).click();
        webDriver.findElement(By.linkText("Электроника")).click();
    }

    @Test
    public void task2_1(){
        webDriver.findElement(By.linkText("Мобильные телефоны")).click();
        webDriver.findElement(By.linkText("Все фильтры")).click();
        webDriver.findElement(By.id("glf-pricefrom-var")).sendKeys("20000");
        webDriver.findElement(By.linkText("Apple")).click();
        webDriver.findElement(By.linkText("Samsung")).click();
        webDriver.findElement(By.linkText("Показать подходящие")).click();
        webDriver.findElement(By.className("button__text"));
        String s = webDriver.findElement(By.xpath("/html/body/div[1]/div[5]/div[2]/div[1]/div[2]/div/div[3]/span/button/span")).getText().toString();

        if (s.equals("Показывать по 12")){
            s = webDriver.findElement(By.xpath("/html/body/div[1]/div[5]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).getText().toString();
        }
        else {
            webDriver.close();
        }
        webDriver.findElement(By.xpath("//*[@id=\"header-search\"]")).sendKeys(s);

        webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/noindex/div/div/div[2]/div/div[1]/form/span/span[2]/button")).click();
        String w = webDriver.findElement(By.xpath("/html/body/div[1]/div[5]/div[2]/div/div/div[1]/div[1]/div/h1")).getText().toString();

        Assert.assertTrue(s.equals(w));
    }

    @Test
    public void task2_2(){
        webDriver.findElement(By.linkText("Наушники и Bluetooth-гарнитуры")).click();
        webDriver.findElement(By.linkText("Все фильтры")).click();
        webDriver.findElement(By.id("glf-pricefrom-var")).sendKeys("5000");
        webDriver.findElement(By.linkText("Beats")).click();
        webDriver.findElement(By.linkText("Показать подходящие")).click();
        int s = 12;
        String e = "";

        if (s == (webDriver.findElements(By.xpath("/html/body/div[1]/div[5]/div[2]/div[1]/div[2]/div/div[1]/div")).size())){
            e = webDriver.findElement(By.xpath("/html/body/div[1]/div[5]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).getText().toString();
        }
        else {
            webDriver.close();
        }
        webDriver.findElement(By.xpath("//*[@id=\"header-search\"]")).sendKeys(e);

        webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/noindex/div/div/div[2]/div/div[1]/form/span/span[2]/button")).click();
        String w = webDriver.findElement(By.xpath("/html/body/div[1]/div[5]/div[2]/div/div/div[1]/div[1]/div/h1")).getText().toString();

        Assert.assertTrue(e.equals(w));

    }

    @Test
    public  void task2_3() throws InterruptedException {
        webDriver.findElement(By.linkText("Мобильные телефоны")).click();
        webDriver.findElement(By.xpath("/html/body/div[1]/div[5]/div[1]/div[2]/div[1]/div[1]/div[3]/a")).click();
        Thread.sleep(5000);
        List<WebElement> list =  webDriver.findElements(By.xpath("/html/body/div[1]/div[5]/div[2]/div[1]/div[2]/div/div[1]/div"));
        int n = list.size();
        int previousPrice = 0;
        boolean flag = true;
        String a = "";

        for (int i = 0; i < n; i++) {
           try {
               a = list.get(i).findElement(By.cssSelector("a>div.price")).getText().toString();

           } catch (Throwable ex ){
               System.out.println(ex.getMessage());
           }


            a = a.replace("\u20BD","");
            a = a.replaceAll("\\s+", "");
            int price = Integer.parseInt(a);
            if (price < previousPrice){
                flag = false;
            }
            else {
                previousPrice = price;
            }
        }
        Assert.assertTrue(flag);
    }

    @After
    public void tearDown(){
        if (webDriver != null) {
            webDriver.close();
        }
    }
}
